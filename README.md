# Button and Mouse Game

## Description
Button and mouse are the pet names of my two children. And the idea of this project is to program a game for them, they
can play on my [Picade](https://shop.pimoroni.com/collections/raspberry-pi/products/picade) (an arcade machine based
on the [Raspberry Pi](https://www.raspberrypi.org/) microcontroller and the [RetroPie](https://retropie.org.uk/)
operating system).

To write the game I decided to use the [Pygame Zero](https://pygame-zero.readthedocs.io/en/stable/index.html) library.

The first game idea is that the player needs to collect the letters of his/her name which are scattered over the screen.

## Requirements

* [Python 3](https://www.python.org/downloads/)

## Installation
The following instructions are for Windows systems.

### Create a virtual environment
If you have installed Python 3 on our system `py --version` should work and output the currently installed version. To
create a virtual environment please run the following command which will create a folder named `venv` in the project
directory. If you prefer another name just replace the last parameter.
```shell
py -m venv venv
```

### Enter the virtual environment
Then activate the virtual environment by running
```shell
.\venv\Scripts\activate
```

### Install the dependencies
You should now see a `(venv)` prefix in your command line. Now install the dependencies by running
```shell
pip install -r requirements.txt
```

### Exit the virtual environment
You can exit the virtual environment by running
```shell
deactivate
```

## Usage

To run the game you need to enter the virtual environment so that every needed dependency is available
```shell
.\venv\Scripts\activate
```
And then start the game by running
```shell
python main.py
```


## Authors and acknowledgment
Michael Lemler

### Graphics
The graphics are drawn by my daughter.

### Sounds
The sound files I use are downloaded from [Mixkit](https://mixkit.co/free-sound-effects/game/) and therefore belong to
their [License](https://mixkit.co/license/#sfxFree) model.
