import random

from pgzero.loaders import sounds

from letter import Letter


class Word:
    letters = []

    def __init__(self, word):
        self.letters = []
        for char in word:
            letter = Letter(char)
            x = random.randint(20, 620)
            y = random.randint(20, 460)
            letter.pos = (x, y)
            self.letters.append(letter)

    def draw(self):
        for letter in self.letters:
            letter.draw()

    def collect(self, pos):
        # self.collect_any_letter(pos)
        self.collect_same_letter_at_different_positions(pos)

    def collect_same_letter_at_different_positions(self, pos):
        # find next letter and its index/position
        (next_index, next_letter) = next(elem for elem in enumerate(self.letters) if not elem[1].found)
        for index, letter in enumerate(self.letters):
            if not letter.found and letter.char == next_letter.char:
                if letter.collidepoint(pos):
                    print("Letter found with index", index)
                    letter.found = True
                    letter.pos = ((next_index + 1) * 30, 440)
                    sounds.collect.play()
                    # if letter is from other position switch position in word
                    if index != next_index:
                        self.letters[index], self.letters[next_index] = self.letters[next_index], self.letters[index]
                    break

    def collect_any_letter(self, pos):
        for index, letter in enumerate(self.letters):
            if not letter.found and letter.collidepoint(pos):
                letter.found = True
                letter.pos = ((index + 1) * 30, 440)
                sounds.collect.play()
                break

    def complete(self):
        return all(letter.found for letter in self.letters)
