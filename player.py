import math
import sys

from pgzero.actor import Actor
from config import SPEED

mod = sys.modules['__main__']


class Dino(Actor):

    def __init__(self):
        Actor.__init__(self, "dino")

    def handle_keyboard_input(self):
        if mod.keyboard.right:
            self.x += SPEED
        if mod.keyboard.left:
            self.x -= SPEED
        if mod.keyboard.up:
            self.y -= SPEED
        if mod.keyboard.down:
            self.y += SPEED


class Rocket(Actor):
    x_direction = 0
    y_direction = 1

    def __init__(self):
        Actor.__init__(self, "rocket")

    def handle_keyboard_input(self):
        if mod.keyboard.right:
            self.angle -= SPEED
            self.x_direction = math.cos(math.radians(self.angle - 90))
            self.y_direction = math.sin(math.radians(self.angle + 90))
        if mod.keyboard.left:
            self.angle += SPEED
            self.x_direction = math.cos(math.radians(self.angle - 90))
            self.y_direction = math.sin(math.radians(self.angle + 90))
        if mod.keyboard.up:
            self.x -= self.x_direction * SPEED
            self.y -= self.y_direction * SPEED
        if mod.keyboard.down:
            self.x += self.x_direction * SPEED
            self.y += self.y_direction * SPEED
