from enum import Enum


class GameState(Enum):
    MENU = 1
    PLAYING = 2
    END = 3
