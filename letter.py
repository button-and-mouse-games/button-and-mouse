from pgzero.actor import Actor


class Letter(Actor):
    found = False
    char = None

    def __init__(self, letter, **kwargs):
        self.char = letter.lower()
        image = "char_{}".format(self.char)
        super().__init__(image, **kwargs)
