import sys

from config import BUTTONS
from menu import Menu
from state import GameState

mod = sys.modules['__main__']


class Game:

    def __init__(self):
        self.WIDTH = mod.WIDTH
        self.HEIGHT = mod.HEIGHT
        self.FONT_SIZE = 30
        self.state = GameState.MENU
        self.player = None
        self.word = None
        self.menu = Menu(self)

    def draw_text(self, text, position, highlighted=False):
        if highlighted:
            mod.screen.draw.text(text,
                                 fontsize=self.FONT_SIZE,
                                 center=position,
                                 color=(255, 255, 255),
                                 shadow=(2.0, 2.0),
                                 scolor="blue"
                                 )
        else:
            mod.screen.draw.text(text,
                                 fontsize=self.FONT_SIZE,
                                 center=position,
                                 color=(255, 255, 255))

    def draw(self):
        mod.screen.clear()
        if self.state is GameState.MENU:
            # mod.screen.blit('background', (0, 0))
            self.menu.draw()
        elif self.state is GameState.PLAYING:
            self.player.draw()
            self.word.draw()
        elif self.state is GameState.END:
            self.word.draw()
            self.draw_text('YOU WON!!! Press return to restart', (self.WIDTH / 2, self.HEIGHT / 2))

    def update(self):
        if self.state is GameState.PLAYING:
            self.player.handle_keyboard_input()

            if self.player.x < 40:
                self.player.x = 40
            if self.player.x > 600:
                self.player.x = 600
            if self.player.y < 50:
                self.player.y = 50
            if self.player.y > 430:
                self.player.y = 430

            self.word.collect(self.player.pos)
            if self.word.complete():
                self.state = GameState.END
                mod.sounds.completed.play()

    def handle_key_pressed(self, key):
        if key == mod.keys.ESCAPE:
            sys.exit()

        if self.state is GameState.MENU:
            self.menu.handle_input(key)

        if self.state is GameState.PLAYING:
            if key == mod.keys.O:
                self.word = self.menu.get_selected_word()

        if self.state is GameState.END:
            if key in BUTTONS:
                self.state = GameState.MENU
