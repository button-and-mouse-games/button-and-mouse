import sys
from enum import Enum

from config import BUTTONS, WORDS
from player import Dino, Rocket
from state import GameState
from word import Word

mod = sys.modules['__main__']


class MenuState(Enum):
    PLAYER = 1
    WORD = 2


class Menu:
    def __init__(self, game):
        self.game = game
        self.state = MenuState.PLAYER
        self.players = ["Dino", "Rocket"]
        self.currentPlayerIndex = 0
        self.words = WORDS
        self.currentWordIndex = 0

    def draw(self):
        if self.state is MenuState.PLAYER:
            self.game.draw_text('Choose a player',
                                (self.game.WIDTH / 2, self.game.HEIGHT / 2 - self.game.FONT_SIZE))
            for index, item in enumerate(self.players):
                offset = -90 if index == 0 else 10
                selected = index == self.currentPlayerIndex
                image_prefix = "player_" + item.lower()
                image = image_prefix + "_selected" if selected else image_prefix
                mod.screen.blit(image, (self.game.WIDTH / 2 + offset, self.game.HEIGHT / 2))
        elif self.state is MenuState.WORD:
            self.game.draw_text('Choose a word to play',
                                (self.game.WIDTH / 2, self.game.HEIGHT / 2 - self.game.FONT_SIZE))
            for index, item in enumerate(self.words):
                self.game.draw_text(item,
                                    (self.game.WIDTH / 2, self.game.HEIGHT / 2 + index * self.game.FONT_SIZE),
                                    index == self.currentWordIndex)

    def handle_input(self, key):
        if self.state == MenuState.PLAYER:
            if key == mod.keys.RIGHT:
                self.next_player()
            elif key == mod.keys.LEFT:
                self.previous_player()
            elif key in BUTTONS:
                self.game.player = Dino() if self.currentPlayerIndex == 0 else Rocket()
                self.game.player.pos = (self.game.WIDTH / 2, self.game.HEIGHT / 2)
                self.state = MenuState.WORD
        elif self.state == MenuState.WORD:
            if key == mod.keys.DOWN:
                self.next_word()
            elif key == mod.keys.UP:
                self.previous_word()
            elif key in BUTTONS:
                self.game.word = Word(self.words[self.currentWordIndex])
                self.state = MenuState.PLAYER
                self.game.state = GameState.PLAYING

    def next_player(self):
        self.currentPlayerIndex += 1
        if self.currentPlayerIndex >= len(self.players):
            self.currentPlayerIndex = len(self.players) - 1

    def previous_player(self):
        self.currentPlayerIndex -= 1
        if self.currentPlayerIndex <= 0:
            self.currentPlayerIndex = 0

    def next_word(self):
        self.currentWordIndex += 1
        if self.currentWordIndex >= len(self.words):
            self.currentWordIndex = len(self.words) - 1

    def previous_word(self):
        self.currentWordIndex -= 1
        if self.currentWordIndex <= 0:
            self.currentWordIndex = 0

    def get_selected_word(self):
        return Word(self.words[self.currentWordIndex])
