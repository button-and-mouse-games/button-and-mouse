import sys

mod = sys.modules['__main__']

WORDS = ["Marie", "Jannis"]

SPEED = 2

BUTTONS = {mod.keys.RETURN,
           mod.keys.LCTRL,
           mod.keys.LSHIFT,
           mod.keys.LALT,
           mod.keys.Z,
           mod.keys.X,
           mod.keys.SPACE}

JOYSTICK = {mod.keys.LEFT,
            mod.keys.UP,
            mod.keys.RIGHT,
            mod.keys.DOWN}
