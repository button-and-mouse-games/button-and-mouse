import pgzrun

from game import Game

WIDTH = 640
HEIGHT = 480

game = Game()


def draw():
    game.draw()


def update():
    game.update()


def on_key_down(key):
    game.handle_key_pressed(key)


pgzrun.go()
